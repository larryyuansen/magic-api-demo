# top old version
docker stop magic-api
# remove old magic-api (images/containers)
docker rm magic-api
docker rmi magic-api
# build for new images (name of magic-api)
mv data-magic-api-0.1.jar magic.jar
# build set 
docker build -t magic-api --build-arg MAGIC_ACTIVE="$1" .
# start new container (name of magic-api)
docker run -d --name magic-api -p 9999:9999 magic-api
# cleanup temp resources
rm /data/magic-api/* -rf
